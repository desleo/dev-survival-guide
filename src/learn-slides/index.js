import Title from "./Title.vue";
import AboutMe from "./AboutMe.vue";
import LearnClub from "./LearnClub.vue";
import PracticalTips from "./PracticalTips.vue";
import Imposter from "./Imposter.vue";
import ThankYou from "./ThankYou.vue";

export default {
  Title,
  AboutMe,
  Imposter,
  LearnClub,
  PracticalTips,
  ThankYou,
};
